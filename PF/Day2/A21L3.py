#PF-Tryout

def generate_next_date(day,month,year):
    #Start writing your code here
    next_month = month
    next_year = year        
    if(day < 28 or day == 29):
        next_day = day + 1
    elif(day == 31):
        next_day = 1
        if(month < 12):
            next_month = month + 1
        elif(month==12):
            next_month = 1
            next_year = year + 1
    elif (day == 30 and month in [4, 6, 9, 11]):
        next_day = 1
        next_month = month + 1
    elif (day == 30 and month in [1,3,5,7,8,10,12]):
        next_day = day +1
    elif (day == 28 and month != 2):
        next_day = day + 1
    elif (day == 28 and month == 2):
        if(year % 4 != 0):
            next_day = 1
            next_month = month + 1
        else:
            next_day = day + 1
            
        
    print(next_day,"-",next_month,"-",next_year)


generate_next_date(30,6,2015)
