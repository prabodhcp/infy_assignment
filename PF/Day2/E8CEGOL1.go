//PF-Exer-8
//This verification is based on string match.

package main
import ("fmt")

func main(){
    var finalFee int
    //Write your program logic here
    //Populate the variable: finalFee
    finalFee = calculateFee(25000, 50, 1500)

     //Do not modify the below print statement for verification to work
     fmt.Println(finalFee) 
}

func calculateFee(coursefee, marks, extrafees int) int {
	var scholarship int
	var finalFee int
    scholarship = coursefee * (marks/2)/100
    finalFee = coursefee - scholarship + extrafees
    return finalFee
}
