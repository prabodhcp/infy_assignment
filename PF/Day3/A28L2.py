#PF-Assgn-28

def find_max(num1, num2):
    max_num=-1
    # Write your logic here
    if num1 >= num2:
        return -1
    list = []
    num = num1
    while num <= num2:
        if (9 < num < 100) and (num%5 == 0) and (((num%10)+(num//10))%3 == 0):
            list.append(num)        
        num += 1
    if len(list) == 0:
        return -1
    else:
        max_num = list[len(list)-1] 
    return max_num

#Provide different values for num1 and num2 and test your program.
max_num=find_max(10,15)
print(max_num)
