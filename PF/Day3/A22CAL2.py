##PF-Assgn-22
#def find_leap_years(given_year):

#    # Write your logic here
#    list_of_leap_years = []
#    count = 0
#    while count < 15:
#        if(check_leap(given_year)):
#            list_of_leap_years.append(given_year)
#            count += 1
#        given_year += 1
#    return list_of_leap_years


#def check_leap(year):
#    if (year % 4) == 0:
#       if (year % 100) == 0:
#           if (year % 400) == 0:
#               return True
#           else:
#               return False
#       else:
#           return True
#    else:
#       return False

#list_of_leap_years=find_leap_years(2002)
#print(list_of_leap_years)

#PF-Assgn-22
def find_leap_years(given_year):

    # Write your logic here
    list_of_leap_years = []
    count = 0
    
    while count < 15:
        if (given_year % 4) == 0:
           if (given_year % 100) == 0:
               if (given_year % 400) == 0:
                   leap = True
               else:
                   leap = False
           else:
               leap = True
        else:
           leap = False
        
        if leap == True:
            list_of_leap_years.append(given_year)
            count += 1
        given_year += 1
    return list_of_leap_years

list_of_leap_years=find_leap_years(2002)
print(list_of_leap_years)
