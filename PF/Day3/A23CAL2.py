#PF-Assgn-23
def calculate_bill_amount(gems_list, price_list, reqd_gems,reqd_quantity):
    bill_amount=0
    #Write your logic here
    for i in range(len(reqd_gems)):
        try:
            bill_amount += price_list[(gems_list.index(reqd_gems[i]))] * reqd_quantity[i]
        except:
            return -1
    return bill_amount

#List of gems available in the store
#gems_list=["Emerald","Ivory","Jasper","Ruby","Garnet"]
gems_list = ['Amber', 'Aquamarine', 'Opal', 'Topaz']

#Price of gems available in the store. gems_list and price_list have one-to-one correspondence
#price_list=[1760,2119,1599,3920,3999]
price_list = [4392, 1342, 8734, 6421]

#List of gems required by the customer
#reqd_gems=["Ivory","Emerald","Garnet"]
reqd_gems=['Amber', 'Opal', 'Topaz']

#Quantity of gems required by the customer. reqd_gems and reqd_quantity have one-to-one correspondence
#reqd_quantity=[3,10,12]
reqd_quantity=[2, 1, 3]

bill_amount=calculate_bill_amount(gems_list, price_list, reqd_gems, reqd_quantity)
print(bill_amount)
