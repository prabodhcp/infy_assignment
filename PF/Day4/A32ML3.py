#PF-Assgn-32
def max_visited_speciality(patient_medical_speciality_list,medical_speciality):
    # write your logic here
    keys=medical_speciality.keys()
    maxcnt = cnt = 0
    
    for key in keys:
        cnt = patient_medical_speciality_list.count(key)
        if cnt > maxcnt:
            maxcnt = cnt
            maxkey = key
    
    speciality = medical_speciality[maxkey]

    return speciality

#provide different values in the list and test your program
patient_medical_speciality_list=[301,'P',302, 'P' ,305, 'P' ,401, 'E' ,656, 'E']
medical_speciality={"P":"Pediatrics","O":"Orthopedics","E":"ENT"}
speciality=max_visited_speciality(patient_medical_speciality_list,medical_speciality)
print(speciality)
