#PF-Assgn-33

def find_common_characters(msg1,msg2):
    #Remove pass and write your logic here
    common_chars = ''
    for i in msg1:
        if i != ' ':
            if i in msg2:
                common_chars += i
    if common_chars == '':
        return -1
    else:
        return common_chars

#Provide different values for msg1,msg2 and test your program
msg1="I like Python"
msg2="Java is a very popular language"
common_characters=find_common_characters(msg1,msg2)
print(common_characters)
