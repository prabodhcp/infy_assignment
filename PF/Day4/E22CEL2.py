#PF-Exer-22

def generate_ticket(airline,source,destination,no_of_passengers):
    ticket_number_list=[]
    #Write your logic here
    ticket_id = 101
    for i in range(no_of_passengers):
        str_val=airline+':'+source[:3]+':'+destination[:3]+':'+str(ticket_id)
        ticket_number_list.append(str_val)
        ticket_id += 1
    

    #Use the below return statement wherever applicable
    return ticket_number_list[-5:]

#Provide different values for airline,source,destination,no_of_passengers and test your program
print(generate_ticket("AI","Bangalore","London",7))
