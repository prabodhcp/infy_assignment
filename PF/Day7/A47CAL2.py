#PF-Assgn-47
def encrypt_sentence(sentence):
    #start writing your code here
    vowel = ['a', 'e', 'i', 'o', 'u']
    wd_list = sentence.split()
    for i in range(0,len(wd_list),2):
    	wd_list[i] = wd_list[i][::-1]
    
    for i in range(1,len(wd_list),2):
    	temp = ''
    	for ch in wd_list[i]:
    		if ch not in vowel:
    			temp += ch
    	for ch in wd_list[i]:
    		if ch in vowel:
    			temp += ch
    	wd_list[i] = temp
				
    separator = ' '
    new_sentence = separator.join(wd_list)
    return new_sentence

sentence="The sun rises in the east"
encrypted_sentence=encrypt_sentence(sentence)
print(encrypted_sentence)
