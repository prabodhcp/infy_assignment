#PF-Assgn-50

def sms_encoding(data):
	#start writing your code here
	word_list = data.split()
	vowels = ['a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U']
	new_word_list = []
#    print(word_list)
	for word in word_list:
		consonant_count = 0
		for ch in word:
			if ch not in vowels:
				consonant_count += 1
		if consonant_count != 0:
			for i in vowels:
				word = word.replace(i, '')
#			print(word)
		new_word_list.append(word)
			
	separator = ' '
	new_sentence = separator.join(new_word_list)
	return new_sentence

#data="I love Python"
data = "GOOD DAYS AND BAD DAYS"
print(sms_encoding(data))
