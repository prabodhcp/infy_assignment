#PF-Assgn-48

def find_correct(word_dict):
    #start writing your code here
    correct_count = 0
    almost_correct_count = 0
    wrong_count = 0
    for key in word_dict:
#    	print(key, word_dict[key])
    	if key == word_dict[key]:
    		correct_count += 1
    	elif len(key) != len(word_dict[key]):
    		wrong_count += 1
    	else:
    		mismatch_count = 0
    		for i in range(len(key)):
    			if key[i] != word_dict[key][i]:
    				mismatch_count += 1
#    			print(key[i] , "-->", word_dict[key][i])
    		if mismatch_count <= 2:
    			almost_correct_count += 1
#    			print("almost correct")
    		else:
    			wrong_count += 1
	
    return [correct_count,almost_correct_count,wrong_count]

word_dict={"THEIR": "THEIR","BUSINESS":"BISINESS","WINDOWS":"WINDMILL","WERE":"WEAR","SAMPLE":"SAMPLE"}
print(find_correct(word_dict))
