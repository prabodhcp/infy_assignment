#PF-Assgn-46

def nearest_palindrome(number):
	#start writitng your code here
	checked_number = number+1
	while True:
		chk_number = checked_number
		rev_number = 0
		while chk_number != 0:
			dig = chk_number % 10
			rev_number = rev_number * 10 + dig
			chk_number //= 10

		if rev_number == checked_number:
			return checked_number
		else:
			checked_number += 1


number=12300
print(nearest_palindrome(number))
