#PF-Assgn-41
def find_ten_substring(num_str):

	#Remove pass and write your logic here
	list_ten_substring = []
	for i in range(len(num_str)):
		sum = int(num_str[i])
		seq = str(sum)
		for j in range(i+1, len(num_str)):
#			print(num_str[j])
			if (sum + int(num_str[j])) == 10:
				seq += num_str[j]
				sum = sum + int(num_str[j])
				list_ten_substring.append(seq)
				continue
			elif (sum + int(num_str[j])) > 10:
				continue
			else:
				seq += num_str[j]
				sum = sum + int(num_str[j])
	
	return list_ten_substring
			

num_str="2825302"
print("The number is:",num_str)
result_list=find_ten_substring(num_str)
print(result_list)
