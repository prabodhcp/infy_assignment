#PF-Assgn-16
def make_amount(rupees_to_make,no_of_five,no_of_one):
    five_needed=0
    one_needed=0

    #Start writing your code here
    #Populate the variables: five_needed and one_needed
    change_possible = True
    max_possible_amount = no_of_five*5 + no_of_one
    if (max_possible_amount < rupees_to_make):
        change_possible = False
    else:
        rupee_amount = rupees_to_make
        if(rupees_to_make % 5 == 0 ):
            five_needed = rupees_to_make / 5;
            if (five_needed < no_of_five):
                change_possible = False
            
        else:
            while(rupee_amount > 5):
                five_needed += 1
                rupee_amount -= 5
            while(rupee_amount > 0):
                one_needed += 1
                rupee_amount -= 1
            


    # Use the below given print statements to display the output
    # Also, do not modify them for verification to work

    if(change_possible):
        print("No. of Five needed :", five_needed)
        print("No. of One needed  :", one_needed)
    else:
        print(-1)


#Provide different values for rupees_to_make,no_of_five,no_of_one and test your program
make_amount(28,8,5)
