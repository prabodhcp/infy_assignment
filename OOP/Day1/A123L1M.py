#OOPR-Assgn-3
#Start writing your code here
class Customer:
	def __init__(self, _name, _total_bill_amount=0):
		self.bill_amount = _total_bill_amount
		self.customer_name = _name

	def purchases(self):
		self.bill_amount *= 0.95

	def pays_bill(self, amount):
		self.bill_amount = amount
		self.purchases()
		print(self.customer_name + "pays bill amount of Rs. " + str(self.bill_amount))
		
c1 = Customer("Raju")
c1.pays_bill(1000)
