#OOPR-Assgn-5
#Start writing your code here
class Vehicle:
	def __init__(self, _vehicle_id, _vehicle_type, _vehicle_cost):
		self.__vehicle_id = _vehicle_id
		self.__vehicle_type = _vehicle_type
		self.__vehicle_cost = _vehicle_cost
		
		
	def calculate_premium(self):
		if self.__vehicle_type == "Two Wheeler":
			self.__premium_amount = 0.02 * self.__vehicle_cost
		elif self.__vehicle_type == "Four Wheeler":
			self.__premium_amount = 0.06 * self.__vehicle_cost
	
	def display_vehicle_details(self):
		print(self.__vehicle_id, self.__vehicle_type, self.__vehicle_cost, self.__premium_amount)
